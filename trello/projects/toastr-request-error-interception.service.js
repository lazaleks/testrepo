(function (){
    "use strict";

    // the appModule is needed to set authInterceptor as a config function
    // look at the last line of this file -> appModule.config(authInterceptorConfig);
    var module = angular.module("app");

    toastrRequestErrorInterceptorConfig.$inject = ["$httpProvider", "$provide"];

    function toastrRequestErrorInterceptorConfig($httpProvider, $provide) {

        $provide.factory("toastrBackendErrorInterceptor", toastrBackendErrorInterceptor);

        toastrBackendErrorInterceptor.$inject = ["$log", "$q","authService","$rootRouter"];

        function toastrBackendErrorInterceptor($log, $q,authService,$rootRouter) {
            return {
                "request": function (config) {
                    return config;
                },
                "requestError": function (rejection) {
                    return $q.reject(rejection);
                },
                "response": function (response) {
                    return response || $q.when(response);
                },
                "responseError": function (rejection) {
                    if (rejection.data && rejection.data.message) {
                        toastr.error(rejection.data.message, "Error occurred!");
                    }
                    return $q.reject(rejection);
                }
            };
        }

        $httpProvider.interceptors.push("toastrBackendErrorInterceptor");
    }




    module.config(toastrRequestErrorInterceptorConfig);


})();
