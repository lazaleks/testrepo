(function(){
    "use strict";

    var module = angular.module("app");

    module.service("ResourceService", service);

    service.$inject = [
        "$http",
        "$q",
        "utils"
    ];

    function service (
        $http,
        $q,
        utils
    ) {

        this.getProject = getProject;
        this.getAllProjects = getAllProjects;
        this.postProject = postProject;
        this.removeProject = removeProject;

        this.getBoard=getBoard;
        this.postBoard=postBoard;
        this.removeBoard=removeBoard;

        this.postList=postList;
        this.removeList = removeList;

        this.postTickets=postTickets;
        this.editTickets = editTickets;
        this.removeTickets = removeTickets;

        this.login = login;
        this.getUser = getUser;



        var baseUrl = "http://localhost:44305/api/";


        function getProject(id) {
            var deferred = $q.defer();

            var url = baseUrl + "projects/" + id;

            $http.get(url).then(function (response) {
                deferred.resolve(response.data);
            }, function(error) {

                deferred.reject(error);
            });
            return deferred.promise;
        }


        function getAllProjects() {

            var deferred = $q.defer();

            var url = baseUrl + "projects";

            $http.get(url).then(function(response) {
                deferred.resolve(response.data);
            }, function(error) {

                deferred.reject(error);
            });

            return deferred.promise;
        }

        function postProject(name) {
            var deferred = $q.defer();

            var url = baseUrl + "projects";

            $http.post(url, { name: name}).then(function (response) {
                deferred.resolve(response.data)
            }, function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        function removeProject(id) {
            var deferred = $q.defer();

            var url= baseUrl + "projects/" + id;


            $http.delete(url,{id:id} ).then(function (response) {
                deferred.resolve(response.data);
            }, function (error) {
                deferred.reject(error)
            });
            return deferred.promise;

        }




        /// Boards

        function getBoard(projectId, boardId) {
            var deferred = $q.defer();

            var url = baseUrl + "projects/" + projectId + "/" + "boards/" + boardId;

            $http.get(url).then(function (response) {
                deferred.resolve(response.data)
            }, function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }


        function postBoard(id,name) {
            var deferred = $q.defer();

            var url = baseUrl + "projects/" + id + "/" + "boards";

            $http.post(url, { name: name}).then(function (response) {
                deferred.resolve(response.data)
            }, function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        function removeBoard(projectId, boardId) {
            var deferred = $q.defer();

            var url= baseUrl + "projects/" + projectId + "/boards/" + boardId;

            $http.delete(url,{id:boardId} ).then(function (response) {
                deferred.resolve(response.data);
            }, function (error) {
                deferred.reject(error)
            });
            return deferred.promise;

        }


        //// List

        function postList(projectId,boardId,name) {
            var deferred = $q.defer();

            var url = baseUrl + "projects/" + projectId + "/" + "boards" + "/" + boardId + "/" + "ticketLists";

            $http.post(url, { name: name}).then(function (response) {
                deferred.resolve(response.data)
            }, function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        function removeList(projectId, boardId, id) {
            var deferred = $q.defer();

            var url= baseUrl + "projects/" + projectId + "/" + "boards" + "/" + boardId + "/" + "ticketLists" + "/" + id;

            $http.delete(url,{id:id} ).then(function (response) {
                deferred.resolve(response.data);
            }, function (error) {
                deferred.reject(error)
            });
            return deferred.promise;

        }



        //// Tickets

        function postTickets(projectId,boardId,listId,name) {
            var deferred = $q.defer();

            var url = baseUrl + "projects/" + projectId + "/" + "boards" + "/" + boardId + "/" + "ticketLists" + "/" + listId + "/"  + "tickets";

            $http.post(url, { name: name}).then(function (response) {
                deferred.resolve(response.data)
            }, function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        function editTickets(projectId, boardId, listId, ticketId, ticketData) {
            var deferred = $q.defer();

            var url= baseUrl + "projects/" + projectId + "/boards/"  + boardId + "/ticketLists/" + listId  + "/tickets/" + ticketId;

            $http.patch(url, ticketData).then(function (response) {
                deferred.resolve(response.data);
            }, function (error) {
                deferred.reject(error)
            });

            return deferred.promise;
        }

        function removeTickets(projectId, boardId, listId, ticketId) {
            var deferred = $q.defer();

            var url= baseUrl + "projects/" + projectId + "/boards/"  + boardId + "/ticketLists/" + listId  + "/tickets/" + ticketId;

            $http.delete(url).then(function (response) {
                deferred.resolve(response.data);
            }, function (error) {
                deferred.reject(error)
            });

            return deferred.promise;
        }



        //// Login

        function login(username,password) {
            var deferred = $q.defer();

            var url = baseUrl + "login";

            $http.post(url,"grant_type=password&username=" + username + "&password=" + password, "application/x-www-form-urlencoded").then(function (response) {
                deferred.resolve(response.data);
            }).catch(function (error) {
                deferred.reject(error)
            });
            return deferred.promise;

        }

        function getUser() {
            var deferred = $q.defer();

            var url = baseUrl + "user";

            $http.get(url).then(function (response) {
                deferred.resolve(response.data);
            }).catch(function (error) {
                deferred.reject(error)
            });
            return deferred.promise;
        }






    }
})();
