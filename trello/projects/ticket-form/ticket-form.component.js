(function (){
    "use strict";

    var module = angular.module("app");

    module.component("ticketForm", {
        templateUrl: "projects/ticket-form/ticket-form.component.html",
        controllerAs: "vm",
        controller: controller,
        bindings: { }
    });

    controller.$inject = ["locals", "close", "ResourceService"];

    function controller(locals, close, ResourceService) {
        var vm = this;

        this.$onInit = function () {
            vm.title = locals.title;
            vm.listName = locals.listName;
            vm.description = locals.description;

            vm.projectId = locals.projectId;
            vm.boardId = locals.boardId;
            vm.listId = locals.listId;
            vm.ticketId = locals.ticketId;


            vm.callback = locals.callback;

        };


        vm.save = save;
        vm.remove = remove;
        vm.closeHandler = closeHandler;



        /// implementation /////////

        function closeHandler() {
            if(close) close();
        }


        function save() {
            var payload = {
                title: vm.title,
                description: vm.description
            };

            ResourceService.editTickets(vm.projectId, vm.boardId, vm.listId, vm.ticketId, payload).then(function (response) {
                close(response && response.data);
            }).catch(function (error) {

            })
        }

        function remove() {
            ResourceService.removeTickets(vm.projectId, vm.boardId, vm.listId, vm.ticketId).then(function (response) {
                close("removed");
            }).catch(function (error) {

            })
        }




    }

})();






